/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.dip.prog.webdrivertest;

import com.codeborne.selenide.ElementsCollection;
import static com.codeborne.selenide.Selenide.$$;

public class SearchResultsPage {

    public ElementsCollection getResults() {
        return $$("#ires li.g");
    }
}
